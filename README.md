# 4square Demo

Este es un proyecto base en Android Studio que contiene la configuración de Retrofit y un archivo llamado FoursquareRequest.java que contiene algunas constantes junto con las llaves necesarias para consumir información del API de Foursquare. Así mismo, contiene la definición de un Service de Retrofit con la firma del método GET que nos ayudará a obtener un listado de venues para la categoría de "Comida" de Foursquare. 

1. Crear la conexión hacia el servicio de búsquedas de Foursquare.
    - La coordenada de la ubicación sobre la cuál se deberá generar la búsqueda será: __19.4270231__, __-99.1686937__
    - Los parámetros de __limit__ y __radius__ son opcionales en cuanto al valor que deban tomar.
2. Una vez hecha la conexión de manera exitosa, la información deberá mostrarse en formato de lista. Para ello, hemos mandado a cada aspirante un acceso a un proyecto de Zeplin en el cuál se encuentra el diseño de esta pantalla.
      - No se requiere lanzar pantallas de detalle.
      - No se requiere ninguna otra interacción sobre los elementos del listado.
3. Para la entrega de la solución, deberá hacerse fork del repositorio y posteriormente, enviar el link del repo en el que esté alojado lo implementado. 