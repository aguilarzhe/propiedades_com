package com.propiedades.foursquaredemo.ui

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.view.LayoutInflater
import android.widget.ImageView
import com.propiedades.foursquaredemo.R
import com.propiedades.foursquaredemo.dto.Venues
import com.squareup.picasso.Picasso

/**
 * Adapter to show the venues responded by web service.
 * TODO: Refactor to use bindings
 */
class VenuesAdapter(val venuesList: MutableList<Venues>): RecyclerView.Adapter<VenuesAdapter.MyViewHolder>(){

    class MyViewHolder(view: View): RecyclerView.ViewHolder(view){
        var title : TextView = view.findViewById(R.id.imageTitleTextView) as TextView
        var subtitle : TextView = view.findViewById(R.id.subtitleTextView) as TextView
        var detail : TextView = view.findViewById(R.id.detailTextView) as TextView
        var icon : ImageView = view.findViewById(R.id.imageView) as ImageView
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent?.getContext())
                .inflate(R.layout.recyclerview_item, parent, false)

        return MyViewHolder(itemView)
    }

    fun updateData(newData : List<Venues>){
        venuesList.clear()
        venuesList.addAll(newData)

        notifyDataSetChanged()
    }

    override fun getItemCount(): Int = venuesList.size


    override fun onBindViewHolder(holder: MyViewHolder?, position: Int) {
        val venues : Venues = venuesList.get(position)


        holder?.title?.text =  venues.name
        holder?.subtitle?.text =  venues.type
        holder?.detail?.text =  venues.address

        Picasso.get().load(venues.iconLocation).into(holder?.icon); // TODO: Resolve the access denied to resource, even in browser is not possible.
    }
}