package com.propiedades.foursquaredemo.ui;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.propiedades.foursquaredemo.dto.Venues;
import com.propiedades.foursquaredemo.web.FoursquareRequest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

/**
 * AsycTask that gets the data from Foursquare API and shows the result in the RecyclerView.
 *
 * TODO: Refactor code using interfaces to decouple this functionality.
 */
class VenuesAsyncTask extends AsyncTask<String, List<Venues>, List<Venues>> {
    private static final int radius = 50;
    private static final int limit = 10;
    private ProgressDialog progressDialog;
    private VenuesAdapter venuesAdapter;

    VenuesAsyncTask(Activity context, VenuesAdapter adapter){
        progressDialog = new ProgressDialog(context);
        this.venuesAdapter = adapter;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Obteniendo contenido");
        progressDialog.show();
    }

    @Override
    protected void onPostExecute(List<Venues> venues) {
        super.onPostExecute(venues);

        progressDialog.hide();
        venuesAdapter.updateData(venues);
    }

    @Override
    protected List<Venues> doInBackground(String... strings) {
        Call<JsonObject> data = FoursquareRequest.getDetails(strings[0], radius,limit );
        List<Venues> venuesList = new ArrayList<>();

        try {
            Response<JsonObject> response= data.execute();
            JsonObject body = response.body();

            JsonArray venues = body.getAsJsonObject("response").getAsJsonArray("venues");
            for(int i = 0; i < venues.size(); i++){
                JsonObject venueJsonObject = venues.get(i).getAsJsonObject();


                String name = (i+1)+ " " + venueJsonObject.get("name").getAsString();
                String type = venueJsonObject.getAsJsonArray("categories").get(0).getAsJsonObject().get("shortName").getAsString();
                String address = venueJsonObject.getAsJsonObject("location").getAsJsonArray("formattedAddress").get(0).getAsString();

                String icon = venueJsonObject.getAsJsonArray("categories").get(0).getAsJsonObject().getAsJsonObject("icon").get("prefix").getAsString() + venueJsonObject.getAsJsonArray("categories").get(0).getAsJsonObject().getAsJsonObject("icon").get("suffix").getAsString();

                venuesList.add(new Venues(name, type, address, icon));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return venuesList;
    }
}
