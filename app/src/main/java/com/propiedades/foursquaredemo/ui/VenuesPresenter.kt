package com.propiedades.foursquaredemo.ui

import android.app.Activity

interface VenuesPresenter{
    fun showVenues(context: Activity, adapter: VenuesAdapter)
}

class MyVenuesPresenter : VenuesPresenter {

    override fun showVenues(context: Activity, adapter: VenuesAdapter) {
        val asyncTask = VenuesAsyncTask(context, adapter)
        asyncTask.execute(getLatLong())
    }

    // TODO: Change it to get device location
    private fun getLatLong(): String = "19.4270231, -99.1686937"
}