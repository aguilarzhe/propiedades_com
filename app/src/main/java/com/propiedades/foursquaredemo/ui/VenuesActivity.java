package com.propiedades.foursquaredemo.ui;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.propiedades.foursquaredemo.R;
import com.propiedades.foursquaredemo.dto.Venues;

import java.util.ArrayList;

public class VenuesActivity extends AppCompatActivity {

    private VenuesPresenter venuesPresenter = new MyVenuesPresenter();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_venues);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerview);
        VenuesAdapter venuesAdapter = new VenuesAdapter(new ArrayList<Venues>());
        recyclerView.setAdapter(venuesAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recyclerView.setHasFixedSize(true);

        venuesPresenter.showVenues(this, venuesAdapter);
    }


}
