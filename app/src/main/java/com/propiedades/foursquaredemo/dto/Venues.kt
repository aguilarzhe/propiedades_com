package com.propiedades.foursquaredemo.dto

data class Venues(var name : String, var type: String, var address: String, var iconLocation: String)